#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "personnage.h"
#include "monsters.h"
#include "shop.h"
#include "Process.h"
#include "items.h"
#include "fight.h"
#include "text.h"

//Les Armes

void Epee_tincelle()
{
    persoCrit + 1;
    persoDamMax + 3;
    persoDamMin +2;
}

void Epee_tard()
{
    persoCrit + 0;
    persoDamMax +6;
    persoDamMin +2;
}

void Epee_toche()
{
    persoCrit + 4;
    persoDamMax +4;
    persoDamMin +1;
}



//Les armures

void Plastron_epite()
{
    persoPV + 10;
    defense + 3;
}


void Plastron_alivre()
{
    persoPV + 30;
    defense + 7;
}


void Plastron_Divin()
{
    persoPV + 50;
    defense + 10;
}


//Bouclier


void Bouclier_tepre()
{
    defense + 5;
    persoPV + 5;
}


void Bouclier_Ashu()
{
    defense + 10;
    persoPV + 10;
}


void Bouclier_Nephro()
{
    defense + 15;
    persoPV + 10;
}


