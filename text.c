#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "personnage.h"
#include "monsters.h"
#include "shop.h"
#include "Process.h"
#include "items.h"
#include "fight.h"
#include "text.h"

int menu_kaleron()
{
    int choix = 0;
    printf("   Bienvenue dans Kaleron\n");
    printf("=============================\n\n");
    printf("Que voulez vous faire ?\n");
    printf("1 : Aller combattre un monstre\n2 : Acceder au shop\n3 : Votre personnage\n\n");
    printf("=============================\n\n");
    scanf("%d", &choix);
    system("cls");
    return choix;
}


int menu_fight()
{
    int choix = 0;
    printf("Menu combat\n\n");
    printf("===========\n\n");
    printf("1 : Orc Asme\n");
    printf("2 : Troll patrol\n");
    printf("3 : Geant nain\n\n");
    printf("===========\n\n");
    printf("Qui veux-tu combattre ?");
    printf("Ton choix : ");
    scanf("%d", &choix);
    system("cls");
    return choix;
}


void menu_personnage()
{
    printf("================\n\n");
    printf("Votre personnage\n\n");
    printf("================\n\n");
    printf("****Statistique****\n");
    printf("**    PV : %d    **\n");
    printf("**  Attaque : %d **\n");
    printf("** Critique : %d **\n");
    printf("*******************\n");
    getch();
    system("cls");

}


int text_shop_sword()
{
    int choix = 0;
    printf("*******Shop********\n");
    printf("* 1 : Epee Tincel *\n");
    printf("* 2 : Epee Tard   *\n");
    printf("* 3 : Epee Toche  *\n");
    printf("*******************\n");
    scanf("%d", &choix);

    return choix;
}


int text_shop_armor()
{
    int choix = 0;
    printf("*********Shop**********\n");
    printf("* 1 : Plastron Epite  *\n");
    printf("* 2 : Plastron Alivre *\n");
    printf("* 3 : Plastron Divin  *\n");
    printf("***********************\n");
    scanf("%d", &choix);

    return choix;
}


int text_shop_shield()
{
    int choix = 0;
    printf("*********Shop**********\n");
    printf("* 1 : Bouclier Tepre  *\n");
    printf("* 2 : Bouclier Ashu   *\n");
    printf("* 3 : Bouclier Nephro *\n");
    printf("***********************\n");
    scanf("%d", &choix);

    return choix;
}
