#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "personnage.h"
#include "monsters.h"
#include "shop.h"
#include "Process.h"
#include "items.h"
#include "fight.h"
#include "text.h"

//Generateur d'or/xp
void generateXpOr()
{
    srand(time(NULL));
    xp = (rand() % (monsterXP - 1 + 1)) + 1;
    or = (rand() % (monsterOr - 1 + 1)) + 1;

    persoCoin = or + persoCoin;
    persoXp = xp + persoXp;
    printf("Vous avez gagne %d d'or et %d d'experience\n", or, xp);
}


//Passage de niveau
void lvlUP()
{
    if (persoXp == jaugeXP)
    {
        persoDamMax = persoDamMax + 2;
        persoDamMin = persoDamMin + 1;
        persoPV = persoPV + 5;
        defense = defense +1;
        jaugeXP = jaugeXP * 2;
        lvl++;
        printf("Tu es passe niveau %d", lvl);
        getch();
    }

}


//Process Attack

void ProcessAttack()
{
    do
    {

    monsterAttack();
    persoAttack();
    text_cbt_coups();
    if (attackM < attackP)
        {
        monsterPV = monsterPV - attackP;
        printf("\n\n**Vous avez inflige %d en degats, il lui reste encore %d PV**\n\n", attackP, monsterPV);
        }
    else if (attackM > attackP)
        {
            persoPV = persoPV - attackM;
            printf("\n\n**Vous avez pris %d de degats, il te reste encore %d PV**\n\n", attackM, persoPV);
        }
        else
            printf("\n\nA n'y rien comprendre vous avez tape tout deux a cote !\n\n");


    }while (persoPV > 0, monsterPV > 0);
}


//Defini les degats du perso ou du monstre

void monsterAttack()
{
   srand(time(NULL));
   attackM = (rand() % (monsterDamMax - monsterDamMin + 1)) + persoDamMin;
   critM = (rand() % (monsterDamMax - monsterDamMin + 1)) + persoDamMin;
   if (monsterCrit == attackM)
      attackM = monsterCrit + attackM;
    else
        attackM = attackM;

}


void persoAttack()
{
   srand(time(NULL));
   attackP = (rand() % (persoDamMax - persoDamMin + 1)) + persoDamMin;
   critP = (rand() % (persoDamMax - persoDamMin + 1)) + persoDamMin;
    if (critP == attackP)
       attackP = persoCrit + attackP;
     else
        attackP = attackP;
}
