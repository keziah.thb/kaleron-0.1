#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "personnage.h"
#include "monsters.h"
#include "shop.h"
#include "Process.h"
#include "items.h"
#include "fight.h"
#include "text.h"

int menu_shop()
{
    int choix = 0;
    printf("\tBienvenue dans le shop\n");
    printf("=============================\n\n");
    printf("Que voulez vous acheter ?\n");
    printf("1 : Acceder au shop des Epees\n2 : Acceder au shop des Armures\n3 : Acceder au shop des Boucliers\n\n");
    printf("=============================\n\n");
    printf("Entrez votre choix : ");
    scanf("%d", &choix);
    switch (choix)
    {
        case 1 :
                sword();
            break;
        case 2 :
                armor();
            break;
        case 3 :
                shield();
            break;
        default :
                printf("Vous n'avez pas bien selectionner\n");
            break;
    }

    system("cls");
    return choix;
}



void sword()
{

    switch (text_shop_sword())
    {
    case 1 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 2 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 3 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    }

}


void armor()
{

    switch (text_shop_armor())
    {
    case 1 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 2 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 3 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    }

}


void shield()
{

    switch (text_shop_shield())
    {
    case 1 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 2 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    case 3 :
            printf("Merci de ton achat !\nA bientot\n");
        break;

    }

}
