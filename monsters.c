#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "personnage.h"
#include "monsters.h"
#include "shop.h"
#include "Process.h"
#include "items.h"
#include "fight.h"
#include "text.h"

int monsterPV;
int monsterDamMax;
int monsterDamMin;
int monsterDefense;
int monsterCrit;
int monsterXP;
int monsterOr;


void Orc_Asme()
{
  monsterPV = monsterPV + 50;
  monsterDamMax = monsterDamMax + 8;
  monsterDamMin = monsterDamMin + 4;
  monsterDefense = monsterDefense + 3;
  monsterCrit = monsterCrit + 3;
  monsterXP = monsterXP + 10;
  monsterOr = monsterOr + 5;
}


void Troll_Patrol()
{
  monsterPV = monsterPV + 75;
  monsterDamMax = monsterDamMax + 10;
  monsterDamMin = monsterDamMin + 6;
  monsterDefense = monsterDefense + 5;
  monsterCrit = monsterCrit + 4;
  monsterXP = monsterXP + 15;
  monsterOr = monsterOr + 8;
}


void Geant_nain()
{
  monsterPV = monsterPV + 100;
  monsterDamMax = monsterDamMax + 14;
  monsterDamMin = monsterDamMin + 8;
  monsterDefense = monsterDefense + 7;
  monsterCrit = monsterCrit + 5;
  monsterXP = monsterXP + 20;
  monsterOr = monsterOr + 14;
}


